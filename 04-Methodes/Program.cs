﻿using System;
using System.Text;

namespace _04_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode 
            int r1 = Somme(1, 2);

            // Appel de methode (sans retour)
            Afficher(r1);
            Afficher(2 * Somme(4, 5));

            // Exercice maximum
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"maximum={Maximum(a, b)}");

            // Exercice paire
            Console.WriteLine(Paire(3));
            Console.WriteLine(Paire(4));

            // Passage par valeur (par défaut en c#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int val = 23;
            TestParamValeur(val);
            Console.WriteLine(val);

            StringBuilder str = new StringBuilder("Hello");
            TestParamValeurReference(str);
            Console.WriteLine(str);


            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            int v1 = 10;
            TestParamRef(ref v1);
            Console.WriteLine(v1);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            int r;
            TestParamOut(1, 3, out r, out int p);
            Console.WriteLine(r);
            Console.WriteLine(p);

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(2, 3, out _, out int p2);
            Console.WriteLine(p2);

            // Paramètre optionnel
            TestValOptionnelle(10);
            TestValOptionnelle(12, "world");
            TestValOptionnelle(12, "world", true);

            // Paramètres nommés
            TestValOptionnelle(str: "world", b: true, a: 12);
            TestValOptionnelle(12, b: true);

            // Exercice Echange
            int a1 = 12;
            int a2 = 34;
            Console.WriteLine($"a1={a1} a2={a2}");
            Echange(ref a1, ref a2);
            Console.WriteLine($"a1={a1} a2={a2}");

            // Exercice Tableau
            int[] t2 = SaisirTab();
            AfficherTab(t2);
            CalculTab(t2, out int minimum, out int maximum, out double moyenne);
            Console.WriteLine($"Minimum={minimum} Maximum={maximum} Moyenne={moyenne}");

            // Nombre de paramètres variable
            double avg = Moyenne(1);
            double avg2 = Moyenne(1, 2);
            double avg3 = Moyenne(1, 2, 4, 5);
            Console.WriteLine($"{avg} {avg2} {avg3}");

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande
            foreach (string s in args)
            {
                Console.WriteLine(s);
            }

            // Surcharge de méthode
            Console.WriteLine(Multiplier(1, 2));    // Correspondance exacte des type des paramètres
            Console.WriteLine(Multiplier(1.5, 2.4));
            Console.WriteLine(Multiplier(1, 2.4));
            Console.WriteLine(Multiplier(1, 10L));  // Pas de correspondance exacte => convertion automatique de long en double => appel de la méthode avec 1 entier et 1 double en paramètres
            Console.WriteLine(Multiplier(1L, 10L));
            //Console.WriteLine(Multiplier(1.5M, 10M));   // Erreur pas de conversion possible

            // Méthode récursive
            int res = Factorial(3);
            Console.WriteLine(res);
            Console.ReadKey();
        }

        static int Somme(int v1, int v2)
        {
            return v1 + v2; //  L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite

        }

        static void Afficher(int a)    // void => pas de valeur retourné
        {
            Console.WriteLine(a);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int val1, int val2)
        {
            return val1 < val2 ? val2 : val1;

            // ou
            //if (val1 < val2)
            //{
            //    return val2;
            //}
            //else
            //{
            //    return val1;
            //}
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Paire(int v)
        {
            //if (v % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            // Ou
            // return v % 2 == 0?true:false;
            // Ou
            return v % 2 == 0; // ou (v & 1) == 0

            // 3 0011 
            // 1 0001
            //   0001   => nombre impaire

            // 4 0100
            // 1 0001
            //   0000   => nombre paire
        }
        #endregion

        #region Passage de paramètres

        // Passage par valeur
        static void TestParamValeur(int i)
        {
            Console.WriteLine(i);
            i = 45;     // La modification de la valeur du paramètre x n'a pas d'influence en dehors de la méthode
            Console.WriteLine(i);
        }

        // Passage par valeur d'un objet
        static void TestParamValeurReference(StringBuilder sb)
        {
            // sb = new StringBuilder("world");
            Console.WriteLine(sb);
            sb.Append("-----------------"); // On peut modifier le contenue de l'objet
            Console.WriteLine(sb);
        }

        // Passage par référence => ref
        static void TestParamRef(ref int v)
        {
            Console.WriteLine(v);
            v = 123;
            Console.WriteLine(v);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(int a, int b, out int sum, out int prod)
        {
            // Console.WriteLine(res);   // erreur 
            sum = a + b;                 // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine(sum);
            prod = a * b;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestValOptionnelle(int a, string str = "hello", bool b = false)
        {
            Console.WriteLine($"{a} {str} {b}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int a, params int[] parvar)
        {
            double somme = a;
            foreach (var v in parvar)
            {
                somme += v;
            }
            return somme / (1 + parvar.Length);
        }
        #endregion

        #region Exercice Echange
        static void Echange(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
        #endregion

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui calcule :
        // - le maximum d’un tableau d’entier
        // - le minimum d’un tableau d’entier
        // - la moyenne
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var v in tab)
            {
                Console.Write($"{v} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tab = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tab[{i}]= ");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tab;
        }

        static void CalculTab(int[] tab, out int min, out int max, out double moyenne)
        {
            min = Int32.MaxValue;
            max = Int32.MinValue;
            double somme = 0.0;
            foreach (var v in tab)
            {
                if (v < min)
                {
                    min = v;
                }
                if (v > max)
                {
                    max = v;
                }
                somme += v;
            }
            moyenne = somme / tab.Length;
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Multiplier(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a * b;
        }

        static double Multiplier(double a, double b)
        {
            Console.WriteLine("2 doubles");
            return a * b;
        }

        static double Multiplier(int a, double b)
        {
            Console.WriteLine("1 entier et 1 double");
            return a * b;
        }
        #endregion

        #region Méthode récursive
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }
}
