﻿using _07_bibliotheque_IO;
using System;

// Dans ce projet, on utilise la bibliothèque _07_bibliotheque_IO
// Pour ajouter un projet de la solution comme dépendance de ce projet
// Clique droit sur le projet -> ajouter -> référence -> on coche le projet que l'on veut utiliser comme dépendance
namespace _08_IO
{

    class Program
    {
        static void Main(string[] args)
        {
            Fichier.InfoLecteur();
            Fichier.InfoDossier();
            Fichier.InfoFichier();

            Fichier.Parcourir(@"C:\Formations\TestIO");

            Fichier.EcrireFichierTexte(@"C:\Formations\TestIO\text.txt");
            Fichier.LireFichierTexte(@"C:\Formations\TestIO\text.txt");

            Fichier.EcrireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            Fichier.LireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");

            Console.ReadKey();
        }
    }
}
