﻿using System;
using System.IO;

// Le projet est une bibliothèque, il est compilé en dll
// c'est un ensemble de classe que l'on va pouvoir utiliser dans un autre projet
namespace _07_bibliotheque_IO
{
    // Par défaut une classe à la visibilitée internal => elle n'est visible qu'à l'interieur de l'assembly (de la dll)
    // Pour qu'elle soit visible à l'extérieur, il faut lui donner la visibilitée public
    public class Fichier
    {

        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drv = DriveInfo.GetDrives();
            foreach (var d in drv)
            {
                Console.WriteLine(d.Name);              // Nom du lecteur
                Console.WriteLine(d.DriveFormat);       // Système de fichiers du lecteur NTFS, FAT 
                Console.WriteLine(d.DriveType);         // Type de lecteur Fixed Removable
                Console.WriteLine(d.TotalSize);         // Espace disponible sur le lecteur
                Console.WriteLine(d.TotalFreeSpace);    // Espace disponible sur le lecteur
            }
        }

        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (!Directory.Exists(@"C:\Formations\TestIO\dddd"))
            {
                Directory.CreateDirectory(@"C:\Formations\TestIO\dddd");    // Création du répertoire
            }
            string[] cheminsDirectory = Directory.GetDirectories(@"C:\Formations\TestIO");  // Liste les répertoires contenu dans le chemin
            foreach (var c in cheminsDirectory)
            {
                Console.WriteLine(c);
            }
            string[] cheminsFiles = Directory.GetFiles(@"C:\Formations\TestIO");    // Liste les fichiers du répertoire
            foreach (var c in cheminsFiles)
            {
                Console.WriteLine(c);
            }
        }

        public static void InfoFichier()
        {
            if (File.Exists(@"C:\Formations\TestIO\f1.txt"))    // Teste si le fichier
            {
                File.Move(@"C:\Formations\TestIO\f1.txt", @"C:\Formations\TestIO\dddd\move.txt");   // Supprime le fichier
            }
        }

        // Sans utiliser Using
        public static void EcrireFichierTexte(string chemin)
        {
            // StreamWriter Ecrire un fichier texte
            StreamWriter sw = new StreamWriter(chemin, true);   // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello World");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        public static void LireFichierTexte(string chemin)
        {
            // Using => Équivalent d'un try / finally + Close()
            using (StreamReader sr = new StreamReader(chemin))  // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream) // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    string str = sr.ReadLine();
                    Console.WriteLine(str);
                }
            }
        }

        public static void EcrireFichierBin(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string path)
        {
            byte[] tab = new byte[10];
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(tab, 0, 10);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    foreach (var v in tab)      // Read => retourne le nombre d'octets lue dans le fichier                     {
                        Console.WriteLine(v);
                }
            }
        }

        // Parcourir un dossier de façon recursive
        // On affiche la liste de tous les fichiers et de tous dossiers
        // Si le dossier n'existe pas il est créée
        public static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] fileNames = Directory.GetFiles(path);
                foreach (string f in fileNames)
                {
                    Console.WriteLine(f);
                }
                string[] directoryNames = Directory.GetDirectories(path);
                foreach (string d in directoryNames)
                {
                    Console.WriteLine($"Répertoire {d}");
                    Console.WriteLine("_____________");
                    Parcourir(d);
                }
            }
            else if (!File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

    }
}
