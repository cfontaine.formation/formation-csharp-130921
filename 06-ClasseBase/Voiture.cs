﻿using System;

namespace _06_ClasseBase
{
    class Voiture
    {
        // Variables d'instances => Etat
        public string marque;
        public string couleur;
        public int vitesse;

        // Méthodes d'instances => comportement
        public void Accelerer(int vacc)
        {
            vitesse += vacc;
        }

        public void Arreter()
        {
            vitesse = 0;
        }

        public bool EstArreter()
        {
            return vitesse == 0;
        }

        public void Afficher()
        {
            Console.WriteLine($"{marque} {couleur} {vitesse}");
        }

    }
}
