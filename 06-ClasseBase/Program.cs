﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _06_ClasseBase
{
    class Program
    {
        static void Main(string[] args)
        {
            Voiture v1 = new Voiture();
            v1.marque = "opel";
            v1.couleur = "rouge";
            v1.vitesse = 0;
            v1.Afficher();
            v1.Accelerer(20);
            v1.Afficher();
            v1.Arreter();
            v1.Afficher();

            Voiture v2 = new Voiture();
            v2.marque = "fiat";
            v2.couleur = "jaune";
            v2.vitesse = 10;

            #region Chaine de caractères
            string str = "Hello";
            string str2 = new string('a', 10);

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length);

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(str[0]);  // H

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            string str3 = str + " World";
            Console.WriteLine(str3);
            Console.WriteLine(string.Concat(str, " World"));

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            string str4 = string.Join(",", "bonjour", "hello", "world");
            Console.WriteLine(str4);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string[] elms = str4.Split(',');
            foreach (string s in elms)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(5));
            // ou de l'indice et pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(5, 2));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5, "-----------"));

            // Supprime une partie de la chaine à partir de l'indice et pour le nombre de caractères passé en paramètre  
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("azerty"));   // false
            Console.WriteLine(str3.StartsWith("Hell"));     // true

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str3.IndexOf('o'));       // 4
            Console.WriteLine(str3.IndexOf('o', 5));    // 7 idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf('o', 8));    // retourne -1, si le caractère n'est pas trouvé

            // Remplace toutes les chaines (ou caratères) oldValue par newValue
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("1234"));
            Console.WriteLine(str3.Contains("Wo"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(45, '_'));
            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(45, '_'));

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("\n \t     azerty yuiop    \n \t".Trim());
            Console.WriteLine("\n \t     azerty yuiop    \n \t".TrimStart());   // idem uniquement en début de chaine
            Console.WriteLine("\n \t     azerty yuiop    \n \t".TrimEnd());     // idem uniquement en fin de chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str3.CompareTo("bonjour")); //1
            Console.WriteLine(string.Compare("bonjour", str3)); //-1

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine("hello".Equals(str));
            Console.WriteLine(str == "Hello");

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToUpper().Substring(6).PadRight(20, '_'));

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédia
            StringBuilder sb = new StringBuilder("Test");
            sb.Append(23.4);
            sb.Append("------------");
            string str5 = sb.ToString();    // Convertion d'un StringBuilder en une chaine de caractères
            Console.WriteLine(str5);
            Console.WriteLine(sb);

            // Exercice Inversion de chaine
            Console.WriteLine(Inverser("Bonjour"));

            // Exercice Palindrome
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("SOS"));
            Console.WriteLine(Palindrome("Radar"));
            #endregion

            #region datetime    
            DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.Hour);

            DateTime fin2021 = new DateTime(2021, 12, 31);
            Console.WriteLine(fin2021);
            TimeSpan fa = fin2021 - d;  // TimeSpan => représente une durée
            Console.WriteLine(fa);

            TimeSpan dixJours = new TimeSpan(10, 0, 0, 0);
            Console.WriteLine(d.Add(dixJours));
            Console.WriteLine(d.AddHours(12));

            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.ToString("dddd MM yy"));
            Console.WriteLine(DateTime.Parse("2020/03/01T10:00:00"));
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("azerty");
            lst.Add(3.5);
            Console.WriteLine(lst[0]);
            if (lst[0] is string str1)
            {
                Console.WriteLine(str1);
            }

            // Collection fortement typée => type générique
            List<string> lstStr = new List<string>();
            lstStr.Add("Hello");
            lstStr.Add("azerty");
            lstStr.Add("Bonjour");
            lstStr.Add("a_effacer");
            // lstStr.Add(5.5);    // On ne peut plus qu'ajouter des chaines de caractères => sinon erreur de compilation

            Console.WriteLine(lstStr[2]);   // Accès à un élément de la liste

            foreach (var s in lstStr)        // Parcourir la collection complétement
            {
                Console.WriteLine(s);
            }

            Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Min());    // Valeur minimum stocké dans la liste
            lstStr.RemoveAt(3);                 // Supprimer le 4ème élément de la liste
            lstStr.Reverse();                   // Inverser l'ordre de la liste
            foreach (var s in lstStr)
            {
                Console.WriteLine(s);
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator<string> it = lstStr.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(34, "azerty");    // Add => ajout d'un valeur associé à une clé
                                    // m.Add(34, "azerty"); // On ne peut pas ajouter, si la clé éxiste déjà => exception
            m.Add(3, "rfanrjnf");
            m.Add(120, "dcvvqrv");
            m.Add(12, "ujjuuu");

            Console.WriteLine(m[120]);   // accès à un élément m[clé] => valeur
            m[3] = "Bonjour";            // Modifier pour la clé 3
            m.Remove(120);               // Supprimer la clé 120 et la valeur associée 
            Console.WriteLine(m.Count);

            // Parcourir un dictionnary
            foreach (var v in m)
            {
                Console.WriteLine($"clé={v.Key} valeur={v.Value}");
            }
            #endregion

            Console.ReadKey();
        }

        // Exercice Inversion de chaine
        // Écrire la fonction Inversee qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // bonjour →  ruojnob
        static string Inverser(string str)
        {
            StringBuilder tmp = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                tmp.Append(str[i]);
            }
            return tmp.ToString();
        }

        // Exercice  Palindrome
        // Écrire une méthode __Palindrome__ qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        //    SOS →  true
        //    Bonjour →  false
        //    radar →  true
        static bool Palindrome(string str)
        {
            string tmp = str.ToLower().Trim();
            return tmp == Inverser(tmp);
        }
    }
}
