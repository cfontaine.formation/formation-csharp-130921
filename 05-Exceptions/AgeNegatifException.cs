﻿using System;

namespace Exceptions
{
    class AgeNegatifException : Exception
    {
        public int Age { get; set; }

        public AgeNegatifException(int age)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message) : base(message)
        {
            Age = age;
        }
    }
}
