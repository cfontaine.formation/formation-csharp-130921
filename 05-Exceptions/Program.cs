﻿using System;

namespace Exceptions
{

    class Program
    {
        static void Main(string[] args)
        {
            bool isNotOK = false;
            do
            {
                try
                {
                    int[] tab = new int[5];
                    int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                    Console.WriteLine(tab[index]);                      // Peut générer une Exception IndexOutOfRangeException , si index est > à la taile du tableau

                    Console.Write("Saisir un age");
                    int age = Convert.ToInt32(Console.ReadLine());      // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                    TraitementEtatCivil(age);
                    Console.WriteLine("suite du traitement");
                    isNotOK = false;
                }

                catch (IndexOutOfRangeException e)  // Attrape les exceptions IndexOutOfRangeException
                {
                    Console.WriteLine("Erreur index en dehors du tableau");
                    Console.WriteLine(e.Message);       // Message => permet de récupérer le messsage de l'exception
                    Console.WriteLine(e.StackTrace);
                }
                catch (FormatException e)     // Attrape les exceptions FormatException
                {
                    Console.WriteLine("Erreur de format");
                    isNotOK = true;
                }
                catch (AgeNegatifException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.Age);
                }
                catch (Exception e) // Attrape tous les autres exception
                {
                    Console.WriteLine("Erreur de saisie");
                    Console.WriteLine(e.Message);
                    isNotOK = true;
                }
                finally  // Le bloc finally est toujours éxécuté. Il est utiliser pour libérer les ressources
                {
                    Console.WriteLine("Toujours executé");
                }

            }
            while (isNotOK);
            Console.WriteLine("fin du programme");
            Console.ReadKey();
        }
        static void TraitementEtatCivil(int age)
        {
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age < -10)       // when : le catch est éxécuté si la condition dans when est vrai
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"{e.Message} : {e.Age}");
                // On relance l'exception
                throw new ArgumentException("Erreur Traiment Employée", e); // e =>référence à l'exception qui a provoquer l'exception
            }
            catch (AgeNegatifException e)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"{e.Message} : {e.Age}");
                throw;  // On relance l'exception pour que l'utilisateur de la méthode la traite à son niveau
            }
        }
        static void TraitementAge(int age)
        {
            Console.WriteLine("début traitement age");
            if (age < 0)
            {
                // throw new Exception($"L'age est négatif ({age})");
                throw new AgeNegatifException(age, "age négatif");   // throw => Lancer un exception
                                                                     // Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traitée par un try/catch
            }                                                       //  si elle n'est pas traiter, aprés la méthode main => arrét du programme
            Console.WriteLine("fin traitement age");
        }
    }
}
