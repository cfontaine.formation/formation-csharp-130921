﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {

            // Condition if
            int a = Convert.ToInt32(Console.ReadLine());
            if (a > 100)
            {
                Console.WriteLine("Le nombre saisie est supérieur à 100");
            }
            else if (a == 100)
            {
                Console.WriteLine("Le nombre saisie est égale à 100");
            }
            else
            {
                Console.WriteLine("Le nombre saisie est inférieur à 100");
            }

            // Exercice: Trie de 2 Valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.WriteLine("Entrer 2 nombres");
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            if (d1 < d2)
            {
                Console.WriteLine($"{d1}<{d2}");
            }
            else if (d1 == d2)
            {
                Console.WriteLine($"{d2}={d1}");
            }
            else
            {
                Console.WriteLine($"{d2}<{d1}");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.WriteLine("Saisir un nombre entier");
            int vi = Convert.ToInt32(Console.ReadLine());
            if (vi > -4 && vi <= 7)
            {
                Console.WriteLine($"{vi} fait parti de l'intervalle -4,7");
            }

            int jours = Convert.ToInt32(Console.ReadLine());
            const int l = 1;
            const int s = 6;
            switch (jours)
            {

                case l:
                    Console.WriteLine("Lundi");
                    break;
                //case s:
                //case s + 1:
                case int v when v >= 6 || v <= 7:  // C# 7.0
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "x":
                case "*":
                    Console.WriteLine($"{v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} / {v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"{op} n'est pas un opérateur valide");
                    break;
            }

            // Opérateur ternaire
            int i1 = Convert.ToInt32(Console.ReadLine());
            int i2 = Convert.ToInt32(Console.ReadLine());
            int max = i1 < i2 ? i2 : i1;
            Console.WriteLine($"maximum={max}");

            // Valeur absolue
            // Saisir un chiffre et afficher la valeur absolue sous la forme | -1.85 | = 1.85
            double valeur = Convert.ToDouble(Console.ReadLine());
            double valeurAbs = valeur < 0.0 ? -valeur : valeur;
            Console.WriteLine($"| {valeur} | = {valeurAbs}");

            // Boucle
            // while
            int i = 0;
            while (i < 10)
            {
                Console.WriteLine($"i={i}");
                i++;
            }

            // do while
            int j = 0;
            do
            {
                Console.WriteLine($"j={j}");
                j++;
            }
            while (j < 10);


            // for
            for (int k = 0; k < 10; k++)
            {
                Console.WriteLine($"k={k}");
            }

            // Instruction de saut
            // break
            for (int k = 0; k < 10; k++)
            {
                if (k == 3)
                {
                    break;  // break => terminer la boucle
                }

                Console.WriteLine($"k={k}");
            }

            // continue
            for (int k = 0; k < 10; k++)
            {
                if (k == 3)
                {
                    continue;   // continue => on passe un itération suivante
                }
                Console.WriteLine($"k={k}");
            }

            for (int k = 0; k < 10; k++)
            {
                for (int l1 = 0; l1 < 5; l1++)
                {
                    if (k == 3)
                    {
                        goto EXIT_LOOP;
                    }
                    Console.WriteLine($"k={k} l={l}");
                }
            }

        EXIT_LOOP: int jour = Convert.ToInt32(Console.ReadLine());
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    //goto case 6;
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Table de multiplication
            // Faire un programme qui affiche la table de multiplication
            // On saisit un nombre entier
            // - le nombre est compris entre 1 et 9(inclu)
            //  on affiche la table de multiplication sous la forme
            //  ```
            //  1 X 4 = 4
            //  2 X 4 = 8
            //    …
            //  9 x 4 = 36
            //  ```
            //  et on recommence la saisie d'un nombre 
            //- le nombre est en dehors de l’intervalle => on arrête le programme
            int m = Convert.ToInt32(Console.ReadLine());
            do
            {
                for (int k = 1; k < 10; k++)
                {
                    Console.WriteLine($"{k} x {m} = {k * m}");
                }
                m = Convert.ToInt32(Console.ReadLine());
            }
            while (m >= 1 && m <= 9);

            // Ou
            for (; ; ) // ou while(true)
            {
                int m2 = Convert.ToInt32(Console.ReadLine());
                if (m2 < 1 || m2 > 9)
                {
                    break;
                }
                for (int k = 1; k < 10; k++)
                {
                    Console.WriteLine($"{k} x {m2} = {k * m2}");
                }
            }

            //Quadrillage
            //On saisit le nombre de colonne, le nombre de ligne et on affiche un quadrillage dans la console sous la forme(ex: pour 2 et 3):

            //[ ][ ]
            //[ ][ ]
            //[ ][ ]
            Console.Write("Saisir le nombre de colonne=");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir le nombre de ligne=");
            int row = Convert.ToInt32(Console.ReadLine());

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ] ");
                }
                Console.WriteLine("");
            }

            Console.ReadKey();
        }
    }
}
