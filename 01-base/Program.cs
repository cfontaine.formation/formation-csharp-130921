﻿using System;
using System.Text;

namespace _01_base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction { NORD = 90, OUEST = 180, SUD = 270, EST = 0 }
    enum Motorisation : short { ESSENCE = 95, DIESEL = 45, GPL = 4, ELECTRIQUE = 220, BIODIESEL = 90 }

    // Énumération comme indicateurs binaires
    [Flags]
    enum Jour
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }

    class Program
    {
        static void Main(string[] args)
        {
            #region Variable et types simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            // Console.WriteLine(i);    // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            double d = 1.23;
            Console.WriteLine(d);

            // Déclaration multiple de variable
            int largeur = 205, hauteur = 23;
            Console.WriteLine(largeur + hauteur);   // addition
            Console.WriteLine(largeur + " hauteur=" + hauteur); // + => concaténation

            // Littétral booléen
            bool test = true; // false
            Console.WriteLine(test);

            // Littéral caractère
            char chr = 'a';
            char chrUtf8 = '\u0045';    // Caractère en UTF-8
            char chrUtf8Hexa = '\x45';
            Console.WriteLine(chr + " " + chrUtf8 + " " + chrUtf8Hexa);

            // Littéral entier -> int par défaut
            int j = 123;
            long l = 234L;  // L-> long 
            uint ui = 123U; // U -> unsigned
            Console.WriteLine(j + " " + l + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 123;          // décimal (base 10) par défaut
            int bin = 0b010101010;  // 0b => binaire(base 2)
            int hexa = 0xFFF;       // 0x => héxadécimal (base 16)
            Console.WriteLine(dec + " " + bin + " " + hexa);


            // Littéral nombre à virgule flottante
            double d2 = 12.3;
            double exp = 1.320e3;
            Console.WriteLine(d2 + " " + exp);

            // Littéral nombre à virgule flottante => par défaut double
            float f = 12.3F;    // F -> Littéral float
            decimal m = 12.3M;  // M -> Littéral decimal:
            Console.WriteLine(f + " " + m);

            // Séparateur _
            int sep = 1_000_000;
            double sep2 = 1_230.67; // pas de séparateur en début, en fin , avant et après la virgule
            Console.WriteLine(sep + " " + sep2);

            // Type implicite -> var
            var v1 = 'a';       // v1 -> char
            var v2 = 1.23F;     // v2 -> float
            Console.WriteLine(v1 + " " + v2);

            // avec @ on peut utiliser les mots réservés comme nom de variable (à éviter)
            int @while = 12;
            Console.WriteLine(@while);
            #endregion

            #region Convertion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur

            short tis = 45;
            int tii = tis;
            double tid = tii;
            Console.WriteLine(tii + "  " + tid);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.3;
            int tei = (int)ted;
            decimal tedc = (decimal)ted;
            Console.WriteLine(tei + "  " + tedc);

            // Dépassement de capacité
            int sh1 = 300;               // 00000000 00000000 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;      //                            00101100    44
            Console.WriteLine(sb1);

            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked

            //checked
            //{
            //    sb1 = (sbyte)sh1;
            //    Console.WriteLine(sh1);
            //}
            unchecked
            {
                sb1 = (sbyte)sh1;
            }
            Console.WriteLine(sh1 + " " + sb1);

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 34;
            double cnv1 = Convert.ToDouble(fc1);    // Convertion d'un entier en double

            string str = "123";
            int cnv2 = Convert.ToInt32(str);    // Convertion d'une chaine de caractère  en entier
            Console.WriteLine(cnv2);

            // Conversion d'une chaine de caractères en entier
            // Parse
            int cnv3 = Int32.Parse(str);
            // cnv3= Int32.Parse("azerty");     //  Erreur => génère une exception
            Console.WriteLine(cnv3);

            // TryParse
            int cnv4;
            bool res = Int32.TryParse(str, out cnv4);    // Retourne true et la convertion est affecté à cnv4
            // res = Int32.TryParse("azerty", out cnv4);    // Erreur => retourne false, 0 est affecté à cnv4
            Console.WriteLine(res + " " + cnv4);
            #endregion

            #region Type référence
            StringBuilder s1 = new StringBuilder("hello");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + "  " + s2);
            s2 = s1;    // s1 et s2 référence le même objet
            Console.WriteLine(s1 + "  " + s2);
            s1.Append(" world");
            Console.WriteLine(s1 + "  " + s2);
            s2.Append(" !!!");
            Console.WriteLine(s1 + "  " + s2);
            s1 = null;
            Console.WriteLine(s1 + "  " + s2);
            s2 = null; // s1 et s2 sont égales à null
            // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            s2 = new StringBuilder("hello2");
            Console.WriteLine(s1 + "  " + s2);
            #endregion

            #region type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            int? tn = null;

            Console.WriteLine(tn.HasValue); // tn == null retourne false
            tn = 123;                       // Conversion implicite (int vers nullable)
            Console.WriteLine(tn.HasValue); // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            Console.WriteLine(tn.Value); // Pour récupérer la valeur, on peut utiliser la propriété Value
            Console.WriteLine((int)tn);  // ou, on peut faire un cast

            #endregion

            #region Constante
            const double PI = 3.14;
            // PI = 2; // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI * 2);
            #endregion

            #region Format de chaine de caractères
            int xi = 2;
            int yi = 4;
            string restFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(restFormat);
            Console.WriteLine("xi {0} yi={1}", xi, yi);    // on peut définir directement le format dans la mèthode WriteLine
            Console.WriteLine($"xi={xi * 2}\t yi={yi}");

            // // Caractères spéciaux
            // // \n       Nouvelle ligne
            // // \r       Retour chariot
            // // \f       Saut de page
            // // \t       Tabulation horizontale
            // // \v       Tabulation verticale
            // // \0       Caractère nul
            // // \a 	    Alerte
            // // \b       Retour arrière
            // // \\       Backslash
            // // \'       Apostrophe
            // // \"       Guillemet

            Console.WriteLine("c:\tmp\newfile");
            // Chaînes textuelles => @ devant une littérale chaine de caractères
            //  => n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile");
            #endregion

            #region Saisir une valeur au clavier
            string strl = Console.ReadLine();
            Console.WriteLine(strl);
            string strl2 = Console.ReadLine();
            int res3 = Convert.ToInt32(strl2);
            Console.WriteLine($"res3={res3}");
            #endregion

            #region Opérateur
            // Operateur arithméthique
            double op1 = 1.2;
            double op2 = 14.8;
            double opRes = op1 + op2;
            Console.WriteLine(opRes);
            int mo = 11 % 2;    // % => modulo (reste de division entière
            Console.WriteLine(mo);

            // Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // - Affiche Bonjour, complété du nom saisie
            Console.WriteLine("Entrer votre nom");
            string nom = Console.ReadLine();
            Console.WriteLine($"Bonjour, {nom}");

            // Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 __ + __ 3 __ = __ 4
            int a1 = Convert.ToInt32(Console.ReadLine());
            int b1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{a1} + {b1} = {a1 + b1}");

            // Moyenne
            // Saisir 2 nombres __entiers__ et afficher la moyenne dans la console
            int c = Convert.ToInt32(Console.ReadLine());
            int e = Convert.ToInt32(Console.ReadLine());
            double moyenne = (double)(c + e) / 2;
            Console.WriteLine(moyenne);

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            int result = ++inc; // inc=1 result=1
            Console.WriteLine($"{inc}  {result}");

            // Post-incrémentation
            inc = 0;
            result = inc++; // result =0 inc=1
            Console.WriteLine($"{inc}  {result}");

            // Affectation composée
            inc = 17;
            int aa = 23;
            inc += aa;  // équivaut à inc=inc+10.0:
            Console.WriteLine(inc);
            inc *= 2;  // équivaut à inc=inc*2:
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool tst1 = inc > 100;  // Une comparaison a pour résultat un booléen
            Console.WriteLine(tst1);    // false
            bool tst2 = inc == 40;
            Console.WriteLine(tst2);    // true

            // Opérateur logique
            Console.WriteLine(!tst1);   // true
            Console.WriteLine(!(inc == 40));    // false

            // Opérateur court-circuit && et ||
            // && => dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tst3 = inc > 100 && inc < 40;  // comme inc > 100 est faux, inc < 40 n'est pas évalué
            Console.WriteLine(tst3);

            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool tst4 = inc == 40 || inc < 50;  // comme inc == 40  est vraie, inc < 50 n'est pas évalué
            Console.WriteLine(tst4);

            // Opérateur Binaires (bit à bit)
            byte b = 0b10010;
            Console.WriteLine(Convert.ToString(~b, 2));          // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b1011, 2));  // et => 0010
            Console.WriteLine(Convert.ToString(b | 0b1011, 2));  // ou => 11011
            Console.WriteLine(Convert.ToString(b ^ 0b1011, 2));  // ou exclusif => 11001
            Console.WriteLine(Convert.ToString(b << 2, 2));      // Décalage à gauche de 2 => 1001000
            Console.WriteLine(Convert.ToString(b >> 1, 2));      // Décalage à droite de 1 => 1001


            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str6 = "azerty";
            string resStr6 = str6 ?? "Default";
            Console.WriteLine(resStr6);         // azerty

            str6 = null;
            resStr6 = str6 ?? "Default";        // Default
            Console.WriteLine(resStr6);
            #endregion

            #region Promotion numérique
            int prn1 = 11;
            double prn2 = 45.6;
            // prn1 est promu en double => Le type le + petit est promu vers le +grand type des deux
            var resPrn = prn1 + prn2;
            Console.WriteLine(resPrn);

            int res7 = prn1 / 2; //5
            double res8 = prn1 / 2.0; //5.5 prn1 est promu en double
            Console.WriteLine($"{res7} {res8}");

            sbyte sb11 = 12;
            sbyte sb22 = 34;    // sbyte, byte, short, ushort, char sont promus en int
            int sb33 = sb11 + sb22; // b1 et b2 sont promus en int
            Console.WriteLine(sb33);
            #endregion


            #region Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.EST;

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string dirStr = dir.ToString();
            Console.WriteLine(dirStr);

            // enum ->  entier (cast)
            int iDir = (int)dir;
            Console.WriteLine(iDir);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Direction dir2 = (Direction)Enum.Parse(typeof(Direction), "NORD");
            Console.WriteLine(dir2);

            //dir = (Direction)Enum.Parse(typeof(Direction), "SUD2");    // Si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(dir);

            // entier =>enum
            iDir = 180;
            if (Enum.IsDefined(typeof(Direction), iDir))
            {
                Direction dir3 = (Direction)iDir;
                Console.WriteLine(dir3);
            }

            // Indicateur Binaire
            Jour rdv = Jour.LUNDI | Jour.MARDI;
            if ((rdv & Jour.LUNDI) != 0)     // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            rdv = rdv | Jour.WEEKEND;
            if ((rdv & Jour.SAMEDI) != 0)       // teste la présence de SAMEDI
            {
                Console.WriteLine("Samedi");
            }
            if ((rdv & Jour.MERCREDI) != 0)
            {
                Console.WriteLine("Mecredi");
            }

            string strJour = rdv.ToString();
            Console.WriteLine(strJour);

            switch (dir)
            {
                case Direction.NORD:
                    Console.WriteLine("Direction nord");
                    break;
                case Direction.SUD:
                    Console.WriteLine("Direction sud");
                    break;
                default:
                    Console.WriteLine("Autre direction");
                    break;
            }
            #endregion


            #region Structure
            Point p1;
            p1.X = 10;      // accès au champs X de la structur
            p1.Y = 2;

            Console.WriteLine($"x={p1.X} y={p1.Y}");

            Point p2 = p1;
            Console.WriteLine($"x={p2.X} y={p2.Y}");

            if (p1.X == p2.X && p1.Y == p2.Y)
            {
                Console.WriteLine("Les points sont égaux");
            }

            #endregion

            Console.ReadKey();
        }
    }
}
