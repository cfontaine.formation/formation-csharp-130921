﻿using System;

namespace _00_helloworld
{

    /// <summary>
    /// La classe Hello World
    /// </summary>
    class Program
    {
        /*
         * Commentaire sur 
         * plusieurs ligne 
         */

        /// <summary>
        /// Le point d'entrée du programme
        /// </summary>
        /// <param name="args">arguments de la ligne de commande</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world"); // Commentaire fin de ligne
            Console.ReadKey();
        }
    }
}
