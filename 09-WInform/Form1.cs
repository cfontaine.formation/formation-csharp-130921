﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _09_WInform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bp1_Click(object sender, EventArgs e)
        {
            DialogResult dr=MessageBox.Show("J'ai cliquer sur le bouton1","Titre",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if(dr== DialogResult.Yes)
            { 
                MessageBox.Show("J'ai choisi oui");
            }
        }

        private void bpAjouter_Click(object sender, EventArgs e)
        {
            ListViewItem item = new ListViewItem(textPrenom.Text, 0);
            item.SubItems.Add(textNom.Text);

            listView1.Items.Add(item);
            textPrenom.Text = "";
            textNom.Text = "";
        }

        private void BpEffacer_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void bp1_MouseEnter(object sender, EventArgs e)
        {  
            Console.WriteLine("Mouse enter");
        }
    }
}
