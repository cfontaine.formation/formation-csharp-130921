﻿
namespace _09_WInform
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bp1 = new System.Windows.Forms.Button();
            this.textPrenom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textNom = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.prenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bpAjouter = new System.Windows.Forms.Button();
            this.BpEffacer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bp1
            // 
            this.bp1.Location = new System.Drawing.Point(396, 367);
            this.bp1.Name = "bp1";
            this.bp1.Size = new System.Drawing.Size(197, 85);
            this.bp1.TabIndex = 0;
            this.bp1.Text = "ok";
            this.bp1.UseVisualStyleBackColor = true;
            this.bp1.Click += new System.EventHandler(this.bp1_Click);
            this.bp1.MouseEnter += new System.EventHandler(this.bp1_MouseEnter);
            // 
            // textPrenom
            // 
            this.textPrenom.Location = new System.Drawing.Point(138, 37);
            this.textPrenom.Name = "textPrenom";
            this.textPrenom.Size = new System.Drawing.Size(150, 22);
            this.textPrenom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Prénom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nom";
            // 
            // textNom
            // 
            this.textNom.Location = new System.Drawing.Point(138, 99);
            this.textNom.Name = "textNom";
            this.textNom.Size = new System.Drawing.Size(150, 22);
            this.textNom.TabIndex = 4;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.prenom,
            this.Nom});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(355, 37);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(403, 286);
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // prenom
            // 
            this.prenom.Text = "Prénom";
            this.prenom.Width = 160;
            // 
            // Nom
            // 
            this.Nom.Text = "Nom";
            this.Nom.Width = 179;
            // 
            // bpAjouter
            // 
            this.bpAjouter.Location = new System.Drawing.Point(138, 367);
            this.bpAjouter.Name = "bpAjouter";
            this.bpAjouter.Size = new System.Drawing.Size(185, 85);
            this.bpAjouter.TabIndex = 6;
            this.bpAjouter.Text = "Ajouter";
            this.bpAjouter.UseVisualStyleBackColor = true;
            this.bpAjouter.Click += new System.EventHandler(this.bpAjouter_Click);
            // 
            // BpEffacer
            // 
            this.BpEffacer.Location = new System.Drawing.Point(692, 367);
            this.BpEffacer.Name = "BpEffacer";
            this.BpEffacer.Size = new System.Drawing.Size(213, 85);
            this.BpEffacer.TabIndex = 7;
            this.BpEffacer.Text = "Effacer";
            this.BpEffacer.UseVisualStyleBackColor = true;
            this.BpEffacer.Click += new System.EventHandler(this.BpEffacer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 586);
            this.Controls.Add(this.BpEffacer);
            this.Controls.Add(this.bpAjouter);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.textNom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textPrenom);
            this.Controls.Add(this.bp1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Annuaire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bp1;
        private System.Windows.Forms.TextBox textPrenom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textNom;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader prenom;
        private System.Windows.Forms.ColumnHeader Nom;
        private System.Windows.Forms.Button bpAjouter;
        private System.Windows.Forms.Button BpEffacer;
    }
}

