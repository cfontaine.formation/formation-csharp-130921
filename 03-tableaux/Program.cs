﻿using System;
using System.Text;

namespace _03_tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau une dimmension
            // Déclarer un tableau
            double[] tab1 = new double[4];

            // Valeur d'initialisation par défaut des éléments du tableau
            // entier => 0
            // nombre à virgule flotente => 0.0
            // caractère => '\u0000'
            // booléen  => false
            // référence => null

            // Accèder à un élément du tableau
            Console.WriteLine(tab1[0]);
            tab1[0] = 12.3;
            Console.WriteLine(tab1[0]);

            // Déclaration et initialisation
            string[] tabStr = { "azerty", "hello", "bonjour" };
            Console.WriteLine(tabStr[2]);

            // taille du tableau => Lenght
            Console.WriteLine(tab1.Length);

            // Console.WriteLine(tab1[23]); // si l'on essaye d'accèder à un élément en dehors du tableau => exception

            // Length => Nombre d'élément du tableau
            Console.WriteLine(tab1.Length);


            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < tab1.Length; i++)
            {
                Console.WriteLine($"tab1[{i}]={tab1[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            foreach (var d in tab1) //  var -> d est de type double
            {
                Console.WriteLine($"{d}"); // d uniquement en lecture
            }


            // Pour un tableau d'objet, on peut modifier l'objet par l'intermédiare de la variable du foreach 
            StringBuilder[] tabSb = new StringBuilder[3];
            foreach (var d in tab1) // double
            {
                Console.WriteLine($"{d}");
            }
            tabSb[0] = new StringBuilder("aze");
            tabSb[1] = new StringBuilder("rty");
            tabSb[2] = new StringBuilder("yui");

            foreach (var s in tabSb)
            {
                Console.WriteLine($"{s}");
                s.Append("-----------");
            }

            foreach (var s in tabSb)
            {
                Console.WriteLine($"{s}");
            }

            // Saisir la taille du tableau
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tabI = new int[size];
            foreach (var v in tabI)
            {
                Console.WriteLine($"{v}");
            }

            // Exercice
            // Maximum et Moyenne d'un tableau d'entier
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau


            //int[] t = { -7, -4, -8, -6, -3 }; // 1

            int taille = Convert.ToInt32(Console.ReadLine()); //2

            if (taille > 0)
            {
                int[] t = new int[taille];

                for (int i = 0; i < taille; i++)
                {
                    Console.Write($"t[{i}]=");
                    t[i] = Convert.ToInt32(Console.ReadLine());
                }

                int maximum = Int32.MinValue; // ou t[0];
                double somme = 0.0;
                foreach (var v in t)
                {
                    if (v > maximum)
                    {
                        maximum = v;
                    }
                    somme += v;
                }
                Console.WriteLine($"Maximum={maximum} Moyenne={somme / t.Length}");
            }
            else
            {
                Console.WriteLine($"La taille {taille} du tableau est incorrecte");
            }
            #endregion

            #region Tableau à 2 dimensions

            // Déclaration d'un tableau à 2 dimensions
            char[,] tabChr = new char[3, 4];

            // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tabChr[0, 0]);
            tabChr[0, 0] = 'a';
            Console.WriteLine(tabChr[0, 0]);

            // Nombre d'élement du tableau
            Console.WriteLine(tabChr.Length); //12

            // Nombre de dimension du tableau 
            Console.WriteLine(tabChr.Rank); //2

            // Nombre d'élement par dimension
            // Nombre de ligne
            Console.WriteLine(tabChr.GetLength(0)); // 3
            // Nombre de colonne
            Console.WriteLine(tabChr.GetLength(1)); // 4

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tabChr.GetLength(0); i++)
            {
                for (int j = 0; j < tabChr.GetLength(1); j++)
                {
                    Console.Write($"\t[{tabChr[i, j]} ]");
                }
                Console.Write("\n");
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var v in tabChr)
            {
                Console.WriteLine(v);
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            bool[,] tabB = { { false, false }, { true, false }, { true, true } };
            for (int i = 0; i < tabB.GetLength(0); i++)
            {
                for (int j = 0; j < tabB.GetLength(1); j++)
                {
                    Console.Write($"\t{tabB[i, j]}");
                }
                Console.Write("\n");
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3D = new int[2, 3, 2];
            tab3D[0, 1, 1] = 23;    // Accès à un élément d'un tableau à 3 dimensions
            Console.WriteLine(tab3D.Rank);     // Nombre de dimension du tableau => 3
            for (int i = 0; i < tab3D.Rank; i++)  // afficher la nombre d'élément de chaque dimmension
            {
                Console.WriteLine(tab3D.GetLength(i));
            }

            // Parcourir un tableau à 3 dimensions
            for (int i = 0; i < tab3D.GetLength(0); i++)
            {
                for (int j = 0; j < tab3D.GetLength(1); j++)
                {
                    for (int k = 0; k < tab3D.GetLength(2); k++)
                    {
                        Console.WriteLine(tab3D[i, j, k]);
                    }
                }
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau ou Tableau en escalier
            int[][] tabEsc = new int[3][];
            tabEsc[0] = new int[3];
            tabEsc[1] = new int[4];
            tabEsc[2] = new int[2];

            tabEsc[0][0] = 4;   // Accès à un élément 
            Console.WriteLine(tabEsc[0][0]);

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length); //3
            // Taille de la première ligne
            Console.WriteLine(tabEsc[0].Length); //3
            // Taille de la deuxième ligne
            Console.WriteLine(tabEsc[1].Length); //4
            // Taille de la troisième ligne
            Console.WriteLine(tabEsc[2].Length); //2

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"\t{tabEsc[i][j]}");
                }
                Console.WriteLine("");
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (int[] row in tabEsc)
            {
                foreach (int elm in row)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine("");
            }

            int[][] tabMulti2 = new int[][] { new int[] { 10, 3, 4, 1, 5 }, new int[] { 12, -9 }, new int[] { 1, -1, 12 } };
            foreach (var row in tabMulti2) // row type -> int []
            {
                foreach (var elm in row)
                {
                    Console.WriteLine(elm);
                }
            }


            #endregion
            Console.ReadKey();
        }
    }
}
