﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Windows.Forms;

namespace _18_Annuaire
{
    public partial class Form1 : Form
    {
        private string csvFilePath;
        public Form1()
        {
            InitializeComponent();
            // Permet de récuperer dans l'élément <appSettings> du fichier App.config la valeur associée à la clé pathCsvFile
            csvFilePath = ConfigurationManager.AppSettings.Get("csvFilePath");
            InitListView();
        }

        private void ajouterBtn_Click(object sender, EventArgs e)
        {
            Contact c = SaisirContact();
            if (c != null)
            {
                listView1.Items.Add(ContactToItem(c));
                Contact.AjouterCsv(csvFilePath, c);
            }
        }

        private void modifierBtn_Click(object sender, EventArgs e)
        {
            int select = SelectContact(out Contact c);
            if (select >= 0)
            {
                c = SaisirContact(c);
                if (c != null)
                {
                    listView1.Items[select] = ContactToItem(c);
                    SaveListView();
                }
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            int index = SelectContact(out _);
            if (index >= 0)
            {
                listView1.Items.RemoveAt(index);
                SaveListView();
            }
        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void InitListView()
        {
            List<Contact> contacts = Contact.ChargerCsv(csvFilePath);
            listView1.Items.Clear();
            foreach (Contact c in contacts)
            {
                listView1.Items.Add(ContactToItem(c));
            }
        }

        private void SaveListView()
        {
            List<Contact> contacts = new List<Contact>();
            foreach (ListViewItem item in listView1.Items)
            {
                contacts.Add(ItemToContact(item));
            }
            Contact.EnregistrerCsv(csvFilePath, contacts);
        }

        private int SelectContact(out Contact contact)
        {
            var selected = listView1.SelectedItems;
            if (selected.Count == 0)
            {
                MessageBox.Show("Il n'a pas de ligne sélectionner", "Modifier un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);
                contact = null;
                return -1;
            }
            else
            {
                contact = new Contact(selected[0].SubItems[0].Text, selected[0].SubItems[1].Text, DateTime.Parse(selected[0].SubItems[2].Text), selected[0].SubItems[3].Text);
                return selected[0].Index;
            }
        }

        private Contact SaisirContact(Contact contactInit = null)
        {
            Form2 f = new Form2(contactInit);
            Contact tmp = null;
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                tmp = f.ContactForm;
            }
            f.Close();
            f.Dispose();
            return tmp;
        }

        private Contact ItemToContact(ListViewItem item)
        {
            return new Contact(item.SubItems[0].Text, item.SubItems[1].Text, DateTime.Parse(item.SubItems[2].Text), item.SubItems[3].Text);
        }

        private ListViewItem ContactToItem(Contact contact)
        {
            ListViewItem item1 = new ListViewItem(contact.Nom, 0);
            item1.SubItems.Add(contact.Prenom);
            item1.SubItems.Add(contact.DateNaissance.ToShortDateString());
            item1.SubItems.Add(contact.Email);
            return item1;
        }
    }
}
