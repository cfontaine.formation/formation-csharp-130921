﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _18_Annuaire
{
    public class Contact
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public DateTime DateNaissance { get; set; }

        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime dateNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Email = email;
        }

        public static void AjouterCsv(string chemin, Contact contact)
        {
            using (StreamWriter sw = new StreamWriter(chemin, true))
            {
                string row = string.Join(";", contact.Prenom, contact.Nom, contact.DateNaissance, contact.Email);
                sw.WriteLine(row);
            }
        }

        public static void EnregistrerCsv(string chemin, List<Contact> contacts)
        {
            using (StreamWriter sw = new StreamWriter(chemin))
            {
                foreach (var c in contacts)
                {
                    string row = string.Join(";", c.Prenom, c.Nom, c.DateNaissance, c.Email);
                    sw.WriteLine(row);
                }
            }
        }



        public static List<Contact> ChargerCsv(string chemin)
        {
            List<Contact> contacts = new List<Contact>();
            if (File.Exists(chemin))
            {
                using (StreamReader sr = new StreamReader(chemin))
                {
                    while (!sr.EndOfStream)
                    {
                        string row = sr.ReadLine();
                        string[] tmp = row.Split(';');
                        contacts.Add(new Contact(tmp[0], tmp[1], DateTime.Parse(tmp[2]), tmp[3]));
                    }
                }
            }
            return contacts;
        }
    }
}
